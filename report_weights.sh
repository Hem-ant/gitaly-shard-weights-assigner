if [[ -n "${DRY_RUN}" ]]; then
  echo "DRY_RUN is set, exiting ..."
  exit 0
fi

cat new-weights.prom | curl -iv --data-binary @- "http://${PUSH_GATEWAY}:9091/metrics/job/${RESOURCE}/tier/${TIER}/type/${TYPE}"
