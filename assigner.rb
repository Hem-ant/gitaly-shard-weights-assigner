# frozen_string_literal: true

require 'set'
require 'json'
require 'net/http'

BALANCING_PROMETHEUS_QUERY = 'sort_desc(instance:node_filesystem_avail:ratio{env="<env>",type="gitaly",mountpoint="/var/opt/gitlab",shard=~"default|praefect",monitor="default"} > <cutoff>)'
PRAEFECT_VIRTUAL_STORAGES_QUERY = 'sum(gitaly_praefect_replication_jobs{env="<env>"}) by (virtual_storage, gitaly_storage)'
SHARD_CUTOFF_PERCENTAGE = 0.20 # Select shards whose free disk percentage above this value
TARGET_INSTANCE = ENV['TARGET_INSTANCE']

def log(msg)
  puts "[ASSIGNER] #{msg}"
end

def check_required_envvars
  %w[
    THANOS_HOST
    TARGET_INSTANCE
    TARGET_API_PRIVATE_TOKEN
  ].each do |envvar|
    raise "Missing envvar #{envvar}" unless ENV.key?(envvar)
  end
end

def call_gitlab_api(method, resource, payload = nil)
  uri      = URI("https://#{TARGET_INSTANCE}/api/v4/#{resource}")
  response = request = nil
  headers  = {
    'Private-Token' => ENV['TARGET_API_PRIVATE_TOKEN'],
    'Content-Type' => 'application/json'
  }

  log("Calling GitLab API (method = #{method}, path = #{uri.path}, payload = #{payload.inspect})")

  Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
    case method
    when :get
      request = Net::HTTP::Get.new(uri.path, headers)
    when :put
      request = Net::HTTP::Put.new(uri.path, headers)
      request.body = JSON.generate(payload)
    else
      raise "Unsupported method #{method}"
    end

    response = http.request(request)
  end

  raise "Failed querying GitLab API for #{resource}, body is #{response.body.inspect}" unless response.is_a?(Net::HTTPSuccess)

  JSON.parse(response.body)
end

def environment_prometheus_identifier
  case TARGET_INSTANCE
  when 'gitlab.com' then 'gprd'
  when 'staging.gitlab.com' then 'gstg'
  else raise("Couldn't identify a Prometheus environment for #{TARGET_INSTANCE}")
  end
end

def convert_shard_name_per_app_convention(shard_name)
  if shard_name.include?('praefect')
    shard_name.gsub(/.*?(\d+)$/, 'praefect-file\1')
  elsif shard_name.start_with?('nfs') || shard_name.start_with?('gitaly')
    shard_name
  else
    "nfs-#{shard_name.gsub(/-(\d+)$/, '\1')}"
  end
end

def query_thanos(query)
  uri = URI("http://#{ENV['THANOS_HOST']}/api/v1/query")
  params = {
    query: query,
    time: Time.now.to_f
  }
  uri.query = URI.encode_www_form(params)

  log("Querying Thanos for #{query}")
  response = Net::HTTP.get_response(uri)
  raise 'Failed querying Thanos' unless response.is_a?(Net::HTTPSuccess)

  thanos_response = JSON.parse(response.body)
  raise 'Thanos returned a status that is not success' unless thanos_response['status'] == 'success'

  thanos_response
end

def shards_ordered_by_available_disk_space
  query = BALANCING_PROMETHEUS_QUERY
    .gsub('<env>', environment_prometheus_identifier)
    .gsub('<cutoff>', SHARD_CUTOFF_PERCENTAGE.to_s)

  thanos_response = query_thanos(query)

  thanos_response['data']['result'].map do |item|
    [
      # FQNDs that start with gitaly use the full FQDN as the
      # storage name
      item['metric']['fqdn'].start_with?('gitaly') ? item['metric']['fqdn'] : item['metric']['fqdn'].gsub(/-stor-g.*$/, ''),
      item['value'][1].to_f
    ]
  end
end

def praefect_shards_to_virtual_storage_mapping
  query           = PRAEFECT_VIRTUAL_STORAGES_QUERY.gsub('<env>', environment_prometheus_identifier)
  thanos_response = query_thanos(query)
  mapping         = {}

  thanos_response['data']['result'].each do |item|
    mapping[item['metric']['gitaly_storage']] = item['metric']['virtual_storage']
  end

  mapping
end

def excluded_shards_set
  Set.new(ENV['EXCLUDED_SHARDS'].to_s.split(','))
end

def consolidate_praefect_shards(shards)
  praefect_mapping           = praefect_shards_to_virtual_storage_mapping
  praefect_shard_names       = Set.new(praefect_mapping.keys)
  praefect_shard_percentages = Hash.new { |h, k| h[k] = [] }

  shards.each_with_index do |(shard_name, shard_value), i|
    next unless praefect_shard_names.include?(shard_name)

    shard_virtual_storage = praefect_mapping[shard_name]

    praefect_shard_percentages[shard_virtual_storage] << shard_value

    log("Consolidating shard #{shard_name} as a Praefect shard of virtual storage #{shard_virtual_storage}")
    shards[i] = nil
  end

  praefect_shard_percentages.each do |(virtual_storage, percentages)|
    shards << [virtual_storage, percentages.sum / percentages.length]
  end

  shards.compact.sort_by { |(_, percentage)| percentage }.reverse
end

def shard_weights_per_thanos
  maximum_balanced_shards = (ENV['MAXIMUM_BALANCED_SHARDS'] || 5).to_i

  all_shards = shards_ordered_by_available_disk_space
  log("Available shards are #{all_shards.map(&:first).join(', ')}")

  all_shards = consolidate_praefect_shards(all_shards)
  log("Available shards after Praefect consolidation are #{all_shards.map(&:first).join(', ')}")

  excluded_shards = excluded_shards_set

  log("Excluding shards #{excluded_shards.to_a.join(', ')} from Thanos response ...")
  available_shards = all_shards
    .map { |name, percentage| [convert_shard_name_per_app_convention(name), percentage] }
    .reject { |shard| excluded_shards.include?(shard[0]) }
    .first(maximum_balanced_shards)

  available_space_max_percentage = available_shards[0][1] # The list sorted by shard percentage descendingly.
  available_shards.map! do |(name, percentage)|
    [
      name,
      ((percentage / available_space_max_percentage) * 100).ceil
    ]
  end
  available_shards.to_h
end

def shard_weights_per_the_application
  settings = call_gitlab_api(:get, 'application/settings')

  settings['repository_storages_weighted']
end

def assign_new_weights(weights)
  if ENV['DRY_RUN']
    log("Skipping assigning new weights because DRY_RUN envvar is set")
    return
  end

  if weights.all? { |(_, weight)| weight == 0 }
    log("Welp! All weights ended up being zero, stopping before I mess things up!")
    exit 1
  end

  call_gitlab_api(:put, 'application/settings', {repository_storages_weighted: weights})
end

def notify_slack(old_weights, new_weights)
  return unless ENV['SLACK_WEBHOOK_URL']

  preamble     = "The following Gitaly shards have been assigned these weights:"
  footer       = ":ci_passing: This message was sent from this <#{ENV['CI_JOB_URL']}|CI job>"
  weights_list = new_weights.map do |(shard, weight)|
    next if old_weights[shard] == weight

    "• `#{shard}`: #{weight}% (was #{old_weights[shard]}%)"
  end.compact.join("\n")

  if ENV['DRY_RUN']
    puts "This message would've been sent to Slack"
    puts "URL: #{ENV['SLACK_WEBHOOK_URL']}"
    puts "Channel: #{ENV['SLACK_CHANNEL']}"
    puts '=' * 80
    puts [preamble, weights_list, footer].join("\n")
    return
  end

  uri     = URI(ENV['SLACK_WEBHOOK_URL'])
  request = Net::HTTP::Post.new(uri)

  payload = {
    blocks: [
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: preamble,
        }
      },
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: weights_list,
        }
      },
      {
        type: 'context',
        elements: [
          {
            type: 'mrkdwn',
            text: footer,
          }
        ],
      }
    ],
    channel: ENV['SLACK_CHANNEL']
  }
  request.set_form_data('payload' => JSON.generate(payload))

  Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
    response = http.request(request)
    response.body
  end

  log('Notification sent to Slack')
end

def output_new_weights_as_prometheus_metrics(new_weights)
  file_name = 'new-weights.prom'

  File.open(file_name, 'w') do |f|
    f.write("# HELP gitaly_shard_weight_total The weight assigned to a Gitaly shard from Rails POV\n")
    f.write("# TYPE gitaly_shard_weight_total gauge\n")

    metrics = new_weights.map do |(shard, weight)|
      "gitaly_shard_weight_total{shard_name=\"#{shard}\",env=\"#{environment_prometheus_identifier}\"} #{weight}"
    end.join("\n")

    f.write(metrics)
    f.write("\n")
  end

  log "Prometheus metrics for new weights are in #{file_name}"
end

def get_old_and_new_weights
  old_weights     = shard_weights_per_the_application
  thanos_weights  = shard_weights_per_thanos
  excluded_shards = excluded_shards_set

  new_weights = old_weights.map do |(shard, old_weight)|
    if (new_weight = thanos_weights[shard])
      [shard, new_weight]
    elsif excluded_shards.include?(shard)
      [shard, old_weight]
    else
      # Shards that are neither in the Thanos result nor excluded should be
      # zeroed to avoid "leaking" shards (e.g. shards that no longer make the cut-off).
      [shard, 0]
    end
  end.to_h

  [old_weights, new_weights]
end

def main
  check_required_envvars

  old_weights, new_weights = get_old_and_new_weights
  log("Old weights = #{old_weights}")
  log("New weights = #{new_weights}")

  output_new_weights_as_prometheus_metrics(new_weights)

  if new_weights == old_weights
    log("No changes in the weights, exiting ...")
    exit 0
  end

  assign_new_weights(new_weights)
  notify_slack(old_weights, new_weights)
end

main
